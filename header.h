#ifndef header_H
#define header_H

#include <stdio.h>
#include <windows.h>
#include <stdbool.h>
#include <time.h>
#include <conio.h>
#define border "="
#define bird "0"
#define pipes "|"
#define pipeCount 3

#define spaceKey 32
#define enterKey 13
#define exitKey 27
#define leftArrow 75
#define rightArrow 72

typedef struct tPipe
{
    int XPos;
    int HoleHeight;
    int HoleGapPos;
} pipe;
typedef struct tGame
{
    COORD MaxRes;
    int poin;
    COORD birdPos;
    pipe pipePos[pipeCount];
} Game;

HANDLE hConsole;
CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
WORD saved_attributes;

CONSOLE_SCREEN_BUFFER_INFO csbi;
int WindowXSize, WindowYSize;

void csrs(void);
void gotoxy(int x, int y);
int randBeetween(int min, int max);
void printc(char Pesan[], int *ypos);
void initGame(Game *Data);
void initSystem();
bool colliding(Game *Data);
void PrintSpace(int size);
DWORD WINAPI printBird(LPVOID lpParam);
DWORD WINAPI controlBird(LPVOID lpParam);
void PrintEndScreen();
void startMenu();

#endif