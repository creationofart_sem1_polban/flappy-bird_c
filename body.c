#include "header.h"

void csrs(void)
{
    COORD pos = {0, 1};
    SetConsoleCursorPosition(hConsole, pos);
}
void gotoxy(int x, int y)
{
    COORD c;
    c.X = x;
    c.Y = y;
    SetConsoleCursorPosition(hConsole, c);
}

int randBeetween(int min, int max)
{
    srand(time(NULL));
    return rand() % ((max + 1) - min) + min;
}

void printc(char Pesan[], int *ypos)
{
    int usedPos = (WindowXSize - strlen(Pesan)) / 2;
    if (usedPos > 1)
    {
        gotoxy(usedPos, *ypos);
    }
    printf("%s", Pesan);
    (*ypos) += 1;
}

void initGame(Game *Data)
{
    system("cls");
    Data->MaxRes.X = (WindowXSize - 1) / 1.5;
    Data->MaxRes.Y = (WindowYSize - 1) - 3;
    Data->poin = 0;
    Data->birdPos.X = 10;
    Data->birdPos.Y = 10;
    for (int i = 0; i < pipeCount; i++)
    {
        Data->pipePos[i].HoleHeight = randBeetween(3, (Data->poin % 10));
        Data->pipePos[i].HoleGapPos = randBeetween(i, Data->MaxRes.Y - Data->pipePos[i].HoleHeight);
        Data->pipePos[i].XPos = Data->MaxRes.X + (30 * i);
    }
}

void initSystem()
{
    hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    GetConsoleScreenBufferInfo(hConsole, &consoleInfo);
    saved_attributes = consoleInfo.wAttributes;
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    WindowXSize = csbi.srWindow.Right - csbi.srWindow.Left + 1;
    WindowYSize = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
}

bool colliding(Game *Data)
{
    if (Data->birdPos.Y < 1 || Data->birdPos.Y >= Data->MaxRes.Y)
    {
        return true;
    }
    for (int i = 0; i < pipeCount; i++)
    {
        if ((Data->birdPos.X == Data->pipePos[i].XPos))
        {
            if (((Data->birdPos.Y - 1 < Data->pipePos[i].HoleGapPos || Data->birdPos.Y + 1 > (Data->pipePos[i].HoleGapPos + Data->pipePos[i].HoleHeight + 1))))
            {
                return true;
            }
            else
            {
                Data->poin++;
            }
        }
    }
    return false;
}
void PrintSpace(int size)
{
    for (int i = 0; i < size; i++)
    {
        printf(" ");
    }
}

DWORD WINAPI printBird(LPVOID lpParam)
{
    Game *dataGame = (Game *)lpParam;
    int windowsWidth = WindowXSize - 1;

    while (!colliding(dataGame))
    {
        csrs();
        // gotoxy(0, 0);
        for (int ytemp = 0; ytemp < dataGame->MaxRes.Y + 1; ytemp++)
        {
            PrintSpace(windowsWidth / 2 - (dataGame->MaxRes.X / 2));
            for (int xtemp = 0; xtemp < dataGame->MaxRes.X + 1; xtemp++)
            {
                bool space = false;
                bool isBorder = false;
                if (xtemp == 0 || ytemp == 0 || xtemp == dataGame->MaxRes.X || ytemp == dataGame->MaxRes.Y)
                {
                    printf(border);
                    isBorder = true;
                }
                else if (xtemp == dataGame->birdPos.X && ytemp == dataGame->birdPos.Y)
                {
                    SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN);
                    printf(bird);
                }
                else
                {
                    space = true;
                }

                for (int i = 0; i < pipeCount; i++)
                {
                    if (xtemp == dataGame->pipePos[i].XPos && !isBorder)
                    {
                        if ((ytemp < dataGame->pipePos[i].HoleGapPos || ytemp > dataGame->pipePos[i].HoleGapPos + dataGame->pipePos[i].HoleHeight))
                        {
                            SetConsoleTextAttribute(hConsole, FOREGROUND_GREEN);

                            printf(pipes);
                            space = false;
                        }
                        else if (xtemp == dataGame->birdPos.X && ytemp == dataGame->birdPos.Y)
                        {
                            space = false;
                        }
                    }
                }

                if (space)
                {
                    printf(" ");
                }
                SetConsoleTextAttribute(hConsole, saved_attributes);
            }
            printf("\n");
        }
        dataGame->birdPos.Y++;
        for (int i = 0; i < pipeCount; i++)
        {
            dataGame->pipePos[i].XPos--;
            if (dataGame->pipePos[i].XPos <= 0)
            {
                dataGame->pipePos[i].XPos = dataGame->MaxRes.X + randBeetween(0, 10);
                dataGame->pipePos[i].HoleGapPos = randBeetween(0, dataGame->MaxRes.Y - dataGame->pipePos[i].HoleHeight);
            }
        }
        PrintSpace(windowsWidth / 2 - (dataGame->MaxRes.X / 2));
        printf("Tekan SPACEBAR / ENTER untuk melompat");
        PrintSpace(dataGame->MaxRes.X - (38 + 39) + 3);
        printf("Tekan ESC untuk menghentikan permainan\n");
        PrintSpace(windowsWidth / 2 - (dataGame->MaxRes.X / 2));
        printf("Skor : %d", dataGame->poin);

        // Sleep(50);
    }
    ExitThread(0);
    return 0;
}

DWORD WINAPI controlBird(LPVOID lpParam)
{
    COORD *birdPos = (COORD *)lpParam;
    char temp;
Start:
    temp = getch();
    if (temp == spaceKey || temp == enterKey)
    {
        birdPos->Y -= 2;
    }
    else if (temp == exitKey)
    {
        birdPos->Y = -1;
        ExitThread(0);
    }
    goto Start;
    ExitThread(0);
    return 0;
}

void PrintEndScreen()
{
    system("color f0");
    Sleep(100);
    system("color 0f");
    int pos = (WindowYSize / 2) - (18 / 2);

    printc("BBBBBBBBBBBBBBBBB   EEEEEEEEEEEEEEEEEEEEEE       GGGGGGGGGGGGG     OOOOOOOOO     ", &pos);
    printc("B::::::::::::::::B  E::::::::::::::::::::E    GGG::::::::::::G   OO:::::::::OO   ", &pos);
    printc("B::::::BBBBBB:::::B E::::::::::::::::::::E  GG:::::::::::::::G OO:::::::::::::OO ", &pos);
    printc("BB:::::B     B:::::BEE::::::EEEEEEEEE::::E G:::::GGGGGGGG::::GO:::::::OOO:::::::O", &pos);
    printc("  B::::B     B:::::B  E:::::E       EEEEEEG:::::G       GGGGGGO::::::O   O::::::O", &pos);
    printc("  B::::B     B:::::B  E:::::E            G:::::G              O:::::O     O:::::O", &pos);
    printc("  B::::BBBBBB:::::B   E::::::EEEEEEEEEE  G:::::G              O:::::O     O:::::O", &pos);
    printc("  B:::::::::::::BB    E:::::::::::::::E  G:::::G    GGGGGGGGGGO:::::O     O:::::O", &pos);
    printc("  B::::BBBBBB:::::B   E:::::::::::::::E  G:::::G    G::::::::GO:::::O     O:::::O", &pos);
    printc("  B::::B     B:::::B  E::::::EEEEEEEEEE  G:::::G    GGGGG::::GO:::::O     O:::::O", &pos);
    printc("  B::::B     B:::::B  E:::::E            G:::::G        G::::GO:::::O     O:::::O", &pos);
    printc("  B::::B     B:::::B  E:::::E       EEEEEEG:::::G       G::::GO::::::O   O::::::O", &pos);
    printc("BB:::::BBBBBB::::::BEE::::::EEEEEEEE:::::E G:::::GGGGGGGG::::GO:::::::OOO:::::::O", &pos);
    printc("B:::::::::::::::::B E::::::::::::::::::::E  GG:::::::::::::::G OO:::::::::::::OO ", &pos);
    printc("B::::::::::::::::B  E::::::::::::::::::::E    GGG::::::GGG:::G   OO:::::::::OO   ", &pos);
    printc("BBBBBBBBBBBBBBBBB   EEEEEEEEEEEEEEEEEEEEEE       GGGGGG   GGGG     OOOOOOOOO     ", &pos);
    SetConsoleTextAttribute(hConsole, FOREGROUND_RED);
    printc("Game ez gini masih kalah", &pos);
    SetConsoleTextAttribute(hConsole, saved_attributes);
    printc("tekan tombol ENTER untuk kembali ke home screen", &pos);
}

void startMenu()
{
    system("cls");
    int pos = (WindowYSize / 2) - (25 / 2);
    printc("   ___ _ ___               ___ _       _           \n", &pos);
    printc("  |  _| |_  |___________  | _ |_|_____| |   ___    \n", &pos);
    printc(" |  _| | . | . | . | | | | _-| |  _| . |  /__O\\_ \n", &pos);
    printc(" |_| |_|___| __| __|__ | |___|_|_| |___|  \\___/-'\n", &pos);
    printc("           |_| |_|  |__|                          \n", &pos);
    printc("  \n", &pos);
    printc("  \n", &pos);
    printc("  \n", &pos);
    printc("  \n", &pos);
    printc("  \n", &pos);
    printc("  \n", &pos);
    printc("            <-      SELECT     ->                 \n", &pos);
    printc("          _______            _______              \n", &pos);
    printc("         |       |          |       |             \n", &pos);
    printc("         | START |          | SCORE |             \n", &pos);
    printc("         |_______|          |_______|             \n", &pos);
    printc("\n", &pos);
    printc("________________________________________________  \n", &pos);
    printc("////////////////////////////////////////////////  \n", &pos);
    printc("================================================  \n", &pos);
    printc("(c) aryrkt - Sality 37 - JTK 2023\n", &pos);
    printf("\n\n");
    pos++;
    printc("*tips: sesuaikan ukuran windows sebelum bermain untuk mengatur ukuran arena", &pos);
    printc("*tekan tombol apapun untuk melihat perubahan ukuran (refresh)", &pos);
}